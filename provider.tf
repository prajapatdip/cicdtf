provider "aws" {
  region = "ap-south-1"
}

terraform {
  backend "s3" {
    bucket = "s3statebackend0905"
    dynamodb_table = "state-lock"
    key = "global/mystatefile/terraform.tfstate"
    region = "ap-south-1"
    encrypt = true
  }
}